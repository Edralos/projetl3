package com.example.projetl3;


import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ItemRowHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
    TextView name;
    TextView misc;
    ImageView thumb;
    Item item;
    Catalog catalog;
    String key;
    public ItemRowHolder(@NonNull View itemView) {
        super(itemView);
        name = (TextView)itemView.findViewById(R.id.rowItName);
        misc = (TextView)itemView.findViewById(R.id.rowItMisc);
        thumb = (ImageView)itemView.findViewById(R.id.rItThumb);
        itemView.setOnClickListener(this);


    }

    public void bindModel(Catalog catalog, Item item){
        this.catalog = catalog;
        this.key = item.getKey();

        this.item = item;
        name.setText(item.getName());
        thumb.setImageBitmap(item.getThumbnail());
        String msc = "";
        if (item.getBrand()!=null)
            msc += item.getBrand()+"\n";
        for (int i=0; i<item.getCategories().size(); i++)
        {
            if (i%2 == 0){
                msc += item.getCategories().get(i) +"\t";
            }
            else{
                msc += item.getCategories().get(i) +"\n";

            }
        }
        misc.setText(msc);

    }

    @Override
    public void onClick(View v) {
        catalog.setPics(key);

        Intent intent = new Intent(itemView.getContext(),ItemActivity.class);
        intent.putExtra("name", item.getName());
        String[] cats = new String[item.getCategories().size()];
        for (int i = 0; i< cats.length;i++){
            cats[i] = (String) item.getCategories().toArray()[i];
        }
        intent.putExtra("categories", cats);
        intent.putExtra("desc", item.getDesc());
        intent.putExtra("working", item.isWorking());



        int[] tframe = new int[item.getTimeFrame().size()];
        for(int i = 0; i<tframe.length;i++){
            tframe[i] = (int)item.getTimeFrame().toArray()[i];
        }
        intent.putExtra("timeFrame",tframe);



        if (item.getYear() != 0){
            intent.putExtra("year", item.getYear());
        }
        if (item.getBrand() != null)
        {
            intent.putExtra("brand", item.getBrand());
        }
        if (item.getTechDetails() != null){
            String[] tDetails = new String[item.getTechDetails().size()];
            for(int i = 0; i< tDetails.length; i++){
                tDetails[i] = (String) item.getTechDetails().toArray()[i];
            }
            intent.putExtra("tDetails", tDetails);
        }
        intent.putExtra("key",key);
    try {
        itemView.getContext().startActivity(intent);

    }
    catch (Exception e){
        e.printStackTrace();
    }

    }
}
