package com.example.projetl3;

import android.graphics.Bitmap;
import android.util.Pair;

import com.example.projetl3.AsyncRequests.RequestPic;
import com.example.projetl3.AsyncRequests.RequestThumbnail;

import java.util.Dictionary;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;

public class Catalog {
    private Map<String,Item> book = new HashMap<String,Item>();

    public Map<String, Item> getBook() {
        return book;
    }

    public void addItem(String k, Item v){
        book.put(k,v);
    }

    public void setAllThumbs(){

        for (Map.Entry<String,Item> pair:
                book.entrySet()
             ) {
            RequestThumbnail rThumb = new RequestThumbnail();

            rThumb.execute(pair.getKey());

            try {
                pair.getValue().setThumbnail(rThumb.get());



            }
            catch (Exception e) {
                e.printStackTrace();
            }


        }
    }
    public void setPics(String key){
        Item it = book.get(key);
        if (it.getPics() != null) {
            RequestPic rPic = new RequestPic();

            Set<String> str = it.getPics().keySet();
            String[] params = new String[str.size() +1];
            params [0] = key;
            for (int i = 0 ; i<str.size() ; i++)
            {
                params[i+1] = (String) str.toArray()[i];
            }
            rPic.execute(params);

            List<Bitmap> lst = null ;
            try {
                lst = rPic.get();
            } catch (Exception e) {
                e.printStackTrace();
            }
            HashMap<String,Bitmap> mp = new HashMap<String, Bitmap>();
            for (int i = 0; i<str.size(); i++)
            {
                mp.put((String) str.toArray()[i],lst.get(i));
            }

            it.setBitPics(mp);
        }

    }

}
