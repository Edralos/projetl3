package com.example.projetl3;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.PersistableBundle;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.projetl3.AsyncRequests.RequestItem;
import com.example.projetl3.AsyncRequests.RequestPic;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class ItemActivity extends AppCompatActivity {
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item);
        Intent intent = getIntent();
        TextView name = (TextView)findViewById(R.id.itemName);
        TextView desc = (TextView)findViewById(R.id.itDesc);
        TextView cats = (TextView)findViewById(R.id.itCat);
        TextView periode = (TextView)findViewById(R.id.itTFrame);
        TextView brand =(TextView)findViewById(R.id.itBrand);
        TextView year = (TextView)findViewById(R.id.itYear);
        TextView work = (TextView)findViewById(R.id.itWork);
        TextView details = (TextView)findViewById(R.id.itTDetails);

        name.setText(intent.getStringExtra("name"));
        desc.setText(intent.getStringExtra("desc"));
        boolean a =  intent.getBooleanExtra("working", false);
        if (a){
            work.setText("oui");

        }
        else{
            work.setText("non");
        }
        int an = intent.getIntExtra("year",0);
        if (an !=0)
            year.setText(Integer.valueOf(an).toString());
        int[] per = intent.getIntArrayExtra("timeFrame");
        String totPer = Integer.toString(per[0]);
        if(per.length > 1)
        {
            for(int i = 1 ; i< per.length;i++){
                totPer +=", "+ Integer.toString(per[i]);
            }
        }
        periode.setText(totPer);

        brand.setText(intent.getStringExtra("brand"));
        String[] ca = intent.getStringArrayExtra("categories");

        String totCa = ca[0];
        if (ca.length > 1){
            for (int i = 1 ; i<ca.length; i++){
                totCa+= ", " + ca[i];
            }
        }
        cats.setText(totCa);

        String[] td = intent.getStringArrayExtra("tDetails");
        if(td != null){
            String totTD = "* " +td[0];
            if (td.length>1){
                for (int i = 1 ; i<td.length; i++){
                    totTD+="\n* "+td[i];
                }
            }
            details.setText(totTD);
        }

        RequestItem ri = new RequestItem();
        String k = intent.getStringExtra("key");
        ri.execute(k);
        try {
            Item it = ri.get();
            if (it.getPics() != null){
                RequestPic rp = new RequestPic();
                String[] ids = new String[it.getPics().size()+1];
                ids[0] = k;
                String[] legends = new String[it.getPics().size()];
                for(int i = 0; i<it.getPics().size();i++){
                    ids[i+1]=(String)it.getPics().keySet().toArray()[i];
                    legends[i]=(String)it.getPics().values().toArray()[i];
                }
                rp.execute(ids);
                List<Bitmap> bmps = rp.get();
                Bitmap[] bitmaps = new Bitmap[bmps.size()];
                for (int i= 0;i< bmps.size();i++){
                    bitmaps[i] = bmps.get(i);
                }

                RecyclerView rv = (RecyclerView)findViewById(R.id.picRecycler);
                rv.setLayoutManager(new LinearLayoutManager(this));

                rv.addItemDecoration(new DividerItemDecoration(this,LinearLayoutManager.VERTICAL));
                rv.setAdapter(new PicListAdapter(bitmaps,legends));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}

