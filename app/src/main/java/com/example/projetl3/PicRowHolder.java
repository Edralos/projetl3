package com.example.projetl3;

import android.graphics.Bitmap;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class PicRowHolder extends RecyclerView.ViewHolder {
    ImageView pic;
    TextView legend;

    public PicRowHolder(@NonNull View itemView) {
        super(itemView);
        pic = (ImageView) itemView.findViewById(R.id.pic);
        legend = (TextView) itemView.findViewById(R.id.legend);
    }

    public void bindModel(Bitmap bmp,String l){
        legend.setText(l);
        pic.setImageBitmap(bmp);
    }


}
