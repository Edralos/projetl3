package com.example.projetl3;

import android.content.Intent;
import android.content.LocusId;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.example.projetl3.AsyncRequests.RequestThumbnail;

import java.util.ArrayList;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Item implements Comparable<Item>{

    private String key;
    private Bitmap thumbnail;
    private String name;
    private List<String> categories = new ArrayList<String>();
    private String desc;
    private List<Integer> timeFrame = new ArrayList<Integer>();
    private int year;
    private String brand;
    private List<String> techDetails;
    private boolean working = false;
    private Map<String,String> pics;
    private Map<String,Bitmap> bitPics;
    private int sort = 0;

    public void setSort(int sort) {
        if (sort <= 2 && sort>=0)
            this.sort = sort;
    }

    public int getSort() {
        return sort;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    public void setThumbnail(Bitmap thumbnail) {
        this.thumbnail = thumbnail;
    }

    public Map<String, Bitmap> getBitPics() {
        return bitPics;
    }

    public void setBitPics(Map<String, Bitmap> bitPics) {
        this.bitPics = bitPics;
    }

    public Bitmap getThumbnail() {
        return thumbnail;
    }

    public void setName(String name) {
            this.name = name;
    }

    public String getName() {
        return name;
    }

    public void addCategories(String categories) {
        this.categories.add(categories);
    }

    public List<String> getCategories() {
        return categories;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getBrand() {
        return brand;
    }

    public void addPics(String k, String v) {
        if (pics == null)
            pics = new HashMap<String, String>();
        pics.put(k,v);
    }

    public String getPics(String k) {
        if (pics != null)
            return pics.get(k);
        return null;
    }

    public Map<String, String> getPics() {
        return pics;
    }

    public void addTechDetails(String techDetails) {
        if (this.techDetails == null)
            this.techDetails = new ArrayList<String>();
        this.techDetails.add(techDetails);
    }

    public List<String> getTechDetails() {
        return techDetails;
    }

    public void addTimeFrame(int timeFrame) {
        this.timeFrame.add(new Integer(timeFrame));
    }

    public List<Integer> getTimeFrame() {
        return timeFrame;
    }

    public void setWorking(boolean working) {
        this.working = working;
    }

    public boolean isWorking() {
        return working;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getYear() {
        return year;
    }

    @Override
    public int compareTo(Item o) {

            if (sort == 0) {
                if (name.charAt(0) > o.name.charAt(0))
                    return 1;
                else if(name.charAt(0) < o.name.charAt(0))
                    return -1;
                else
                    return 0;
            }


            else{
                if (timeFrame.get(0) > o.timeFrame.get(0))
                    return 1;
                else if (timeFrame.get(0) < o.timeFrame.get(0))
                    return -1;
                else
                    return 0;
            }

    }
}
