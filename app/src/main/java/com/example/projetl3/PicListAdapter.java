package com.example.projetl3;

import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class PicListAdapter extends RecyclerView.Adapter<PicRowHolder> {
    Bitmap[] bitmaps;
    String[] legends;

    public PicListAdapter(Bitmap[] bitmaps, String[] legends){
        this.bitmaps = bitmaps;
        this.legends = legends;
    }

    @NonNull
    @Override
    public PicRowHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new PicRowHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_pic, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull PicRowHolder holder, int position) {
        holder.bindModel(bitmaps[position],legends[position]);
    }

    @Override
    public int getItemCount() {
        return bitmaps.length;
    }
}
