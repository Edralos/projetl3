package com.example.projetl3;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SortedList;

import java.util.List;

public class ItemListAdapter extends RecyclerView.Adapter<ItemRowHolder> {
    Catalog catalog;
    List<Item> list;
    public ItemListAdapter(Catalog catalog, List<Item> list){
        this.catalog = catalog;
        this.list = list;
    }

    @NonNull
    @Override
    public ItemRowHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return  new ItemRowHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_main,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull ItemRowHolder holder, int position) {
        holder.bindModel(catalog,list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}

