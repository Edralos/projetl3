package com.example.projetl3.AsyncRequests;

import android.content.Intent;
import android.os.AsyncTask;
import android.util.JsonReader;

import com.example.projetl3.Catalog;
import com.example.projetl3.Item;

import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URL;
import java.nio.channels.AsynchronousByteChannel;

import javax.net.ssl.HttpsURLConnection;

public class RequestCatalog extends AsyncTask<Void, Void, Catalog> {
    private final String BASE = "https://demo-lia.univ-avignon.fr/cerimuseum/";

    @Override
    protected Catalog doInBackground(Void... voids) {
        URL url;
        HttpsURLConnection connection = null;
        Catalog fin = new Catalog();
        JsonReader reader;
        try {
            url = new URL(BASE + "catalog");
            connection = (HttpsURLConnection) url.openConnection();
            InputStream is = connection.getInputStream();
            reader = new JsonReader(new InputStreamReader(is,"UTF-8"));
            reader.beginObject();
            while (reader.hasNext())
            {
                String key = reader.nextName();
                Item it  = readItem(reader);
                fin.addItem(key,it);
            }
            reader.endObject();
            reader.close();
            connection.disconnect();

        }
        catch (Exception e) {
            e.printStackTrace();
            if (connection != null)
                connection.disconnect();
        }

        return fin;
    }

    private Item readItem(JsonReader reader) throws IOException {
        Item it = new Item();
        reader.beginObject();
        while(reader.hasNext()){
            switch (reader.nextName())
            {
                case "name":
                    it.setName(reader.nextString());
                    break;
                case "categories":
                    reader.beginArray();
                    while(reader.hasNext())
                    {
                        it.addCategories(reader.nextString());
                    }
                    reader.endArray();
                    break;

                case "description":
                    it.setDesc(reader.nextString());
                    break;

                case "timeFrame":
                    reader.beginArray();
                    while(reader.hasNext())
                    {
                        it.addTimeFrame(reader.nextInt());
                    }
                    reader.endArray();
                    break;
                case "year":
                    it.setYear(reader.nextInt());
                    break;

                case "brand":
                    it.setBrand(reader.nextString());
                    break;

                case "technicalDetails":
                    reader.beginArray();
                    while (reader.hasNext())
                    {
                        it.addTechDetails(reader.nextString());
                    }
                    reader.endArray();
                    break;

                case "working":
                    it.setWorking(reader.nextBoolean());
                    break;

                case "pictures":
                    reader.beginObject();
                    while(reader.hasNext())
                    {
                        it.addPics(reader.nextName(), reader.nextString());
                    }
                    reader.endObject();
                    break;

                default:
                    reader.skipValue();
                    break;
            }
        }
        reader.endObject();

        return it;



    }
}
