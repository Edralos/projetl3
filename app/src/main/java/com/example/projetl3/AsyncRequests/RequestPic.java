package com.example.projetl3.AsyncRequests;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

public class RequestPic extends AsyncTask<String, Void, List<Bitmap>> {
    private final String BASE = "https://demo-lia.univ-avignon.fr/cerimuseum/";
    private final int SIZE = 2048;
    @Override
    protected List<Bitmap> doInBackground(String... strings) {
        ArrayList<Bitmap> list = new ArrayList<Bitmap>();
        URL url;
        String id = strings[0];
        HttpsURLConnection connection = null;

            for (String str: strings
            ) {
                Bitmap bitmap;

                if (str.equals(strings[0]))
                    continue;
                try {

                    url = new URL(BASE + "items/" + id + "/images/" + str);
                    connection = (HttpsURLConnection) url.openConnection();
                    InputStream is = connection.getInputStream();
                    bitmap = BitmapFactory.decodeStream(is);
                    list.add(bitmap);
                    is.close();
                    connection.disconnect();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            return list;

    }
}
