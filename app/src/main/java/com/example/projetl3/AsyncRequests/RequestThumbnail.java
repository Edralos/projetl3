package com.example.projetl3.AsyncRequests;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import java.io.InputStream;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class RequestThumbnail extends AsyncTask<String, Void, Bitmap> {
    private final String BASE = "https://demo-lia.univ-avignon.fr/cerimuseum/";


    @Override
    protected Bitmap doInBackground(String... strings) {
        URL url;
        String id = strings[0];
        HttpsURLConnection connection;
        Bitmap bitmap = null;
        try {
            url = new URL(BASE + "items/" + id + "/thumbnail");
            connection = (HttpsURLConnection) url.openConnection();
            InputStream is = connection.getInputStream();
            bitmap = BitmapFactory.decodeStream(is);
            is.close();
            connection.disconnect();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return bitmap;
    }
}
