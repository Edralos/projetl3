package com.example.projetl3.AsyncRequests;

import android.os.AsyncTask;
import android.util.JsonReader;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

public class RequestCategories extends AsyncTask<Void,Void, List<String>> {
    private final String BASE = "https://demo-lia.univ-avignon.fr/cerimuseum/";

    @Override
    protected List<String> doInBackground(Void... voids) {
        URL url;
        HttpsURLConnection connection =null;
        List<String> list = new ArrayList<String>();
        JsonReader reader;
        try{
            url = new URL(BASE + "categories");
            connection = (HttpsURLConnection) url.openConnection();
            InputStream is = connection.getInputStream();
            reader = new JsonReader(new InputStreamReader(is, "UTF-8"));
            reader.beginArray();
            while(reader.hasNext())
            {
                list.add(reader.nextString());
            }
            reader.endArray();
            reader.close();
            connection.disconnect();

        }
        catch (Exception e){
            e.printStackTrace();
            if (connection != null)
                connection.disconnect();
        }
        return list;
    }
}
