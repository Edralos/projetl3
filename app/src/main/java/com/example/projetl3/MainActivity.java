package com.example.projetl3;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Icon;
import android.os.Bundle;

import com.example.projetl3.AsyncRequests.RequestCatalog;
import com.example.projetl3.AsyncRequests.RequestCategories;
import com.example.projetl3.AsyncRequests.RequestThumbnail;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SortedList;

import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;

public class MainActivity extends AppCompatActivity {
    Catalog catalog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        RequestCatalog rcatalog = new RequestCatalog();
        rcatalog.execute();
        
        try {
            catalog = rcatalog.get();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (catalog != null)
        {
            catalog.setAllThumbs();
            for (Map.Entry<String, Item> pair:
                    catalog.getBook().entrySet()) {
                pair.getValue().setKey(pair.getKey());
            }
        }
        final List<Item> itemList = new ArrayList<Item>();

        for (Map.Entry<String, Item> pair:
        catalog.getBook().entrySet()){
            itemList.add(pair.getValue());
        }
        Collections.sort(itemList);

        final RecyclerView rv = (RecyclerView) findViewById(R.id.item_recycler);
        rv.setLayoutManager(new LinearLayoutManager(this));
        rv.addItemDecoration(new DividerItemDecoration(this,LinearLayoutManager.VERTICAL));
        rv.setAdapter(new ItemListAdapter(catalog, itemList));

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (Item it :
                        itemList) {
                    if (it.getSort() == 0){
                        it.setSort(1);
                    }
                    else
                        it.setSort(0);

            }
                Collections.sort(itemList);
                rv.setLayoutManager(new LinearLayoutManager(MainActivity.this));
                rv.addItemDecoration(new DividerItemDecoration(MainActivity.this,LinearLayoutManager.VERTICAL));
                rv.setAdapter(new ItemListAdapter(catalog, itemList));
        }});
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public Catalog getCatalog() {
        return catalog;
    }
}
